module extent.pseudoassert;

import std.stdio;
import std.conv;

bool pseudoassert(bool test, string msg, string file = __FILE__, int line = __LINE__)
{
    if(!test)
    {
        stderr.writeln(file ~ '(' ~ line.to!string ~ "): assert failed: " ~ msg);
        return false;
    }
    return true;
}

bool pseudoassert(bool test, string file = __FILE__, int line = __LINE__)
{
    if(!test)
    {
        stderr.writeln(file ~ '(' ~ line.to!string ~ "): assert failed");
        return false;
    }
    return true;
}
