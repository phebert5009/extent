/**
 * This module contains tools to aid in the serialization of objects
 * directly into bytestreams.
 * It should be able to serialize most objects (exceptions include unions)
 *
 * NOTES:
 * Note that nested classes cannot be deserialized, neither can callables.
 * Note also that this is dependant on the endianness of the system.
 * Do not send a big-endian serialized value to a small-endian machine or vice-versa.
 * (fixing the endianness problem should not be hard)
 */
module extent.serialization;

///
unittest
{
    import std.conv;
    struct S
    {
        int a, b;
        string c;
    }

    string commonText = " is not serializable";
    assert(isSerializable!int, "int" ~ commonText);
    assert(isSerializable!char, "char" ~ commonText);
    assert(isSerializable!float, "float" ~ commonText);
    assert(isSerializable!string, "string" ~ commonText);
    assert(isSerializable!S, "basic struct" ~ commonText);
    assert(isSerializable!C, "basic class" ~ commonText); // IS FALSE...
    commonText = " serialization is not properly implemented";
    void[] k = "hello".serialize();
    assert(deserialize!string(k) == "hello", "string" ~ commonText);
    k = [1,2,3,4].serialize();
    assert(deserialize!(typeof(1)[])(k) == [1,2,3,4], "int[]" ~ commonText);
    k = 'a'.serialize();
    assert(deserialize!(typeof('a'))(k) == 'a', "basic" ~ commonText);
    S s = S(1, 2, "");
    s.c = s.a.to!string ~ ' ' ~ s.b.to!string; // avoid having the string in the .data segment
    k = s.serialize();
    assert(deserialize!S(k) == s, "struct" ~ commonText);
    // C had to be defined outside the unittest.
    C c = new C();
    c.a = 1;
    c.b = 2;
    c.c = c.a.to!string ~ ' ' ~ c.b.to!string; // avoid having the string in the .data segment
    k = c.serialize();
    assert(deserialize!C(k) == c, "class" ~ commonText);
}

import std.traits;
import std.meta;

/**
 * is true if T implements both a void[] serialize(T) function
 * and a T deserialize!(T)(void[]) function
 */
template isSerializable(T)
{
    enum bool isSerializable = is(typeof((inout int i)
    {
        T t = T.init;
        void[] stream = t.serialize;
        T t2 = deserialize!T(stream);
    }));
}

/**
 * Turns a basic item or an array of basic items
 * into a void pointer so that it may be printed to a file
 */
void[] serialize(T)(T item)
if(isBasicType!T)
{
    return cast(void[])[item];
}

/// ditto
void[] serialize(T)(T array)
if(!is(T == void[]) && isArray!T && isBasicType!(ForeachType!T))
{
    return array.length.serialize() ~ cast(void[])array;
}

/// ditto
void[] serialize(T)(T array)
if(is(T == void[]))
{
    return array.length.serialize() ~ array;
}

/**
 * Turns an array of non-basic serializable items
 * (eg. serializable classes/structs, bidimensional arrays)
 * into a void array.
 */
void[] serialize(T)(T array)
if(!is(T == void[]) && isArray!T && !isBasicType!(ForeachType!T) && isSerializable!(ForeachType!T))
{
    void[] ans = array.length.serialize();
    foreach(item; array)
    {
        ans ~= item.serialize();
    }
    return ans;
}


void[] serialize(T)(T object)
if((is(T == class) || is(T == struct)) && allSatisfy!(isSerializable, Fields!T))
{
    void[] ans;
    foreach(name; FieldNameTuple!T)
    {
        ans ~= __traits(getMember, object, name).serialize();
    }
    return ans;
}

/**
 * Turns a serialized item back into it's original form.
 */
T deserialize(T)(ref void[] stream)
if(isBasicType!T)
{
    void[] interest = stream[0..T.sizeof];
    stream = stream[T.sizeof..$];
    return (cast(T[])interest)[0];
}

/// ditto
T deserialize(T)(ref void[] stream)
if(!is(T == void[]) && isArray!T && isBasicType!(ForeachType!T))
{
    alias mutT = Unqual!(ForeachType!T);
    mutT[] ans;
    ans.length = stream.deserialize!(typeof(ans.length));
    size_t byteLength = ans.length * mutT.sizeof;
    ans[] = cast(mutT[])stream[0..byteLength];
    stream = stream[byteLength..$];
    return cast(T)ans;
}

/// ditto
T deserialize(T)(ref void[] stream)
if(is(T == void[]))
{
    void[] ans;
    ans.length = stream.deserialize!(typeof(ans.length));
    size_t byteLength = ans.length;
    ans[] = stream[0..byteLength];
    stream = stream[byteLength..$];
    return ans;
}

/// ditto
T deserialize(T)(ref void[] s)
if(!is(T == void[]) && isArray!T && !isBasicType!(ForeachType!T) && isSerializable!(ForeachType!T))
{
    alias mutT = Unqual!(ForeachType!T);
    mutT[] ans;
    ans.length = s.deserialize!(typeof(ans.length));
    for(int i = 0; i < ans.length; i++)
    {
        ans[i] = s.deserialize!(mutT);
    }
    return cast(T)ans;
}

/// ditto
T deserialize(T)(ref void[] s)
if((is(T == struct)  || is(T == class)) && is(typeof(() { auto ans = new T; return ans; })) && allSatisfy!(isSerializable, Fields!T))
{
    T ans;
    static if(is(T == class)) {
        ans = new T;
    }
    foreach(name; FieldNameTuple!T)
    {
        __traits(getMember, ans, name) = s.deserialize!(typeof(__traits(getMember, ans, name)));
    }
    return ans;
}

version(unittest):
class C
{
    int a, b;
    string c;
    this() {}

    override bool opEquals(Object oo) {
        if(typeid(oo) == typeid(C))
        {
            C o = cast(C)oo;
            return a == o.a && b == o.b && c == o.c;
        }
        return false;
    }
}
