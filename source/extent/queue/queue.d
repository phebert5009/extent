module extent.queue.queue;

import std.uuid;
import extent.serialization;

struct Message
{
    UUID chainId;
    string eventName;
    void[] payload;
}

struct QueueConnection
{
    void sendMessage(Message message)
    {

    }

    void sendMessage(T)(UUID chainId, string eventName, T toSend)
    if(!is(T == message))
    {
        sendMessage(Message(chainId, eventName, toSend.serialize()));
    }

    void sendMessage(T)(UUID chainId, T toSend)
    if(!is(T == message))
    {
        sendMessage(chainId, T.stringof, toSend);
    }

    void sendMessage(T)(string eventName, T toSend)
    if(!is(T == message))
    {
        sendMessage(randomUUID(), eventName, toSend);
    }

    void sendMessage(T)(T toSend)
    if(!is(T == message))
    {
        sendMessage(randomUUID(), T.stringof, toSend);
    }
}

/**
 * Queue service where all data goes through one stream.
 * No routing is done and so listeners must do the filtering themselves.
 */
struct QueueServiceMicro
{
    void main()
    {

    }
}
