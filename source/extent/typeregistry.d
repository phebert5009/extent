module extent.typeregistry;

import extent.serialization;

struct TypeRegistry
{
    void[][TypeInfo] registry;
    void register(T)(T t)
    if(isSerializable!T)
    {
        registry[typeid(T)] = t.serialize();
    }

    T resolve(T)()
    if(isSerializable!T)
    {
        return registry[typeid(T)].deserialize!T();
    }
}
