import std.stdio;
import extent.ecs;
import extent.serialization;

struct Movement
{
    int dx;
    int dy;
}

struct Position
{
    int x;
    int y;

    void move(World* world, Action!"move" action)
    {
        auto movement = action.data.deserialize!Movement();
        x += movement.dx;
        y += movement.dy;
        writefln!"Moved, new position: (%d, %d)"(x, y);
    }
}

void main()
{
    World world;
    auto ent = world.newEntity();
    auto ent2 = world.newEntity();
    Position pos, pos2;
    world.attach(ent, pos);
    world.attach(ent2, pos2);
    Movement movement = Movement(5,4);
    writeln("creating action");
    Action!"move" action;
    action.data = movement.serialize();
    action.entity = ent;
    action.scope_ = ActionScope.local;
    writeln("sending action");
    world.send(action);
    world.send(action);
    writeln("cleaning up");
    world.destroy(ent);
}
